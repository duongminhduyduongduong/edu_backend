import { DataSource } from "typeorm";
import { TableScore } from "./entities/TableScore";
import { User } from "./entities/User";

export const myDataSource = new DataSource({
  type: "mongodb",
  url: `mongodb+srv://${process.env.DB_USERNAME_DEV}:${process.env.DB_PASSWORD_DEV}@cluster0.v0cu3.mongodb.net/edu?retryWrites=true&w=majority`,
  entities: [User, TableScore] /* or [User, ...] */,
  logging: true,
  synchronize: true,
});

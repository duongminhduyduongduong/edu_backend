import { Field, ID, InputType } from "type-graphql";
import { ObjectID } from "typeorm";

@InputType()
export class LogoutInput {
  @Field((_type) => ID)
  _id: ObjectID;
}

import { Request, Response } from "express";
import { UserAuthPayload } from "./User/UserAuthPayload";

export interface Context {
    req: Request
    res: Response
    user: UserAuthPayload
}
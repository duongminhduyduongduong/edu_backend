import { TableScore } from "../../entities/TableScore";
import { Field, ObjectType } from "type-graphql";
import { IMutationResponse } from "../MutationResponse";
import { FieldError } from "../FieldError";

@ObjectType({ implements: IMutationResponse })
export class TableScoreMutationResponse implements IMutationResponse {
  code: number;
  success: boolean;
  message?: string;

  @Field({ nullable: true })
  tableScore?: TableScore;

  @Field((_type) => [FieldError], { nullable: true })
  errors?: FieldError[];
}

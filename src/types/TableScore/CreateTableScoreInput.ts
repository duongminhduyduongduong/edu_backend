import { Field, InputType } from "type-graphql";

@InputType()
export class createTableScoreInput {
  @Field()
  name!: string;

  @Field()
  type!: "plus" | "minus" | "normal";

  @Field()
  multiplier!: string;
}

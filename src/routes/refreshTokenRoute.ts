import express from "express";
import { Secret, verify } from "jsonwebtoken";
import { User } from "../entities/User";
import { UserAuthPayload } from "../types/User/UserAuthPayload";
import { createToken, sendRefreshToken } from "../utils/auth";
import { ObjectId } from "mongodb";

const router = express.Router();

router.get("/", async (req, res) => {
  const refreshToken = req.cookies.refreshToken;

  if (!refreshToken) return res.sendStatus(401);

  try {
    const decodedUser = verify(
      refreshToken,
      process.env.REFRESH_TOKEN_SECRET as Secret
    ) as UserAuthPayload;

    const existingUser = await User.findOne({
      select: ["email", "username", "avatar", "createdAt", "token_version"],
      where: {
        _id: new ObjectId(decodedUser._id),
      },
    });

    if (
      !existingUser ||
      decodedUser.token_version !== existingUser.token_version
    ) {
      return res.sendStatus(401);
    }

    sendRefreshToken(res, existingUser);

    return res.json({
      success: true,
      accessToken: createToken("accessToken", existingUser),
      user: existingUser,
    });
  } catch (err) {
    console.log("Error refreshing token", err);

    return res.sendStatus(403);
  }
});

export default router;

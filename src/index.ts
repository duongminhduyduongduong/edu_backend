require("dotenv").config();
import "reflect-metadata";
import express from "express";
import cookieParser from "cookie-parser";
import cors from "cors";
import { myDataSource } from "./data-source";
import { ApolloServer } from "apollo-server-express";
import { buildSchema } from "type-graphql";
import {
  ApolloServerPluginLandingPageGraphQLPlayground,
  ApolloServerPluginDrainHttpServer,
} from "apollo-server-core";
import { UserResolver } from "./resolvers/User";
import { TableScoreResolver } from "./resolvers/TableScore";
import { createServer } from "http";
import { Context } from "vm";
import refreshTokenRouter from "./routes/refreshTokenRoute";
// import mongoose from "mongoose";

const main = async () => {
  // establish database connection
  myDataSource
    .initialize()
    .then(() => {
      console.log("Data Source has been initialized!");
    })
    .catch((err) => {
      console.error("Error during Data Source initialization:", err);
    });

  // create and setup express app
  const app = express();
  app.use(cookieParser());
  app.use(express.json());
  app.use(
    cors({
      origin: "http://localhost:3000",
      credentials: true,
    })
  );

  app.use("/refresh_token", refreshTokenRouter);

  const httpServer = createServer(app);

  const applloServer = new ApolloServer({
    schema: await buildSchema({
      resolvers: [UserResolver, TableScoreResolver],
      validate: false,
    }),
    plugins: [
      ApolloServerPluginLandingPageGraphQLPlayground(),
      ApolloServerPluginDrainHttpServer({
        httpServer,
      }),
    ],
    context: ({ req, res }): Pick<Context, "req" | "res"> => ({ req, res }),
    cache: "bounded",
    persistedQueries: false,
    introspection: true,
  });

  await applloServer.start();

  applloServer.applyMiddleware({
    app,
    cors: {
      origin: "http://localhost:3000",
      credentials: true,
    },
  });

  const PORT = process.env.PORT;

  app.listen(PORT, () =>
    console.log(
      `Server started on port ${PORT}. GraphQL server started on localhost:${PORT}${applloServer.graphqlPath}`
    )
  );
};

main().catch((error) => console.log(error));

import { Field, ID, ObjectType } from "type-graphql";
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  ObjectID,
  ObjectIdColumn,
  UpdateDateColumn,
} from "typeorm";

@ObjectType()
@Entity()
export class TableScore extends BaseEntity {
  @Field((_type) => ID)
  @ObjectIdColumn()
  _id!: ObjectID;

  @Field()
  @Column()
  name!: string;

  @Field()
  @Column()
  type!: "plus" | "minus" | "normal";

  @Field()
  @Column()
  multiplier!: string;

  @Field()
  @CreateDateColumn()
  createdAt: Date;

  @Field()
  @UpdateDateColumn()
  updatedAt: Date;
}

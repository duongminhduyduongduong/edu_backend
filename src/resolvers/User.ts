import argon2 from "argon2";
import { Context } from "src/types/Context";
import {
  Arg,
  Ctx,
  Mutation,
  Query,
  Resolver,
  UseMiddleware,
} from "type-graphql";
import { FindOneOptions } from "typeorm";
import { User } from "../entities/User";
import { checkAuth } from "../middleware/checkAuth";
import { LoginInput } from "../types/User/LoginInput";
import { RegisterInput } from "../types/User/RegisterInput";
import { UserMutationResponse } from "../types/User/UserMutationResponse";
import { createToken, sendRefreshToken } from "../utils/auth";

@Resolver()
export class UserResolver {
  @Mutation((_return) => UserMutationResponse)
  async register(
    @Arg("regsiterInput") args: RegisterInput
  ): Promise<UserMutationResponse> {
    try {
      const { username, password, email } = args;

      const hashedPassword = await argon2.hash(password);
      const newUser = User.create({
        username,
        email,
        password: hashedPassword,
      });

      await newUser.save();

      return {
        code: 200,
        success: true,
        message: "User create successfully",
        user: newUser,
      };
    } catch (error) {
      console.log(error);
      return {
        code: 500,
        success: false,
        message: `Internal server error, ${error.message}`,
      };
    }
  }

  @Mutation((_return) => UserMutationResponse)
  async login(
    @Arg("loginInput")
    { email, password }: LoginInput,
    @Ctx() { res }: Context
  ): Promise<UserMutationResponse> {
    try {
      const existingUser = await User.findOne({
        where: {
          email,
        },
      });

      if (!existingUser) {
        return {
          code: 400,
          success: false,
          message: "User not found",
        };
      }

      const isPasswordValid = await argon2.verify(
        existingUser.password,
        password
      );

      if (!isPasswordValid) {
        return {
          code: 400,
          success: false,
          message: "Incorrect password",
        };
      }

      sendRefreshToken(res, existingUser);

      return {
        code: 200,
        success: true,
        message: "Logged in successfully",
        user: existingUser,
        accessToken: createToken("accessToken", existingUser),
      };
    } catch (error) {
      console.log(error);
      return {
        code: 500,
        success: false,
        message: `Internal server error, ${error.message}`,
      };
    }
  }

  @UseMiddleware(checkAuth)
  @Mutation((_return) => UserMutationResponse)
  async logout(@Ctx() { res, user }: Context): Promise<UserMutationResponse> {
    const _id = user._id as FindOneOptions<User>;
    const existingUser = await User.findOne(_id);

    if (!existingUser) {
      return {
        code: 400,
        success: false,
        message: "Who are u ?",
      };
    }

    existingUser.token_version += 1;

    await existingUser.save();

    res.clearCookie("refreshToken", {
      httpOnly: true,
      secure: true,
      sameSite: "lax",
      path: "/refresh_token",
    });

    return {
      code: 200,
      success: true,
      message: "Loggout successfully",
    };
  }

  @UseMiddleware(checkAuth)
  @Query((_return) => User, { nullable: true })
  async me(@Ctx() { user }: Context): Promise<User | null> {
    const _id = user._id as FindOneOptions<User>;
    const existingUser = await User.findOne(_id);

    return existingUser;
  }

  @UseMiddleware(checkAuth)
  @Query((_return) => [User], { nullable: true })
  async getAllUsers(): Promise<User[] | null> {
    return User.find();
  }

  // @Query((_return) => User, { nullable: true })
  // async getUserById(
  //   @Arg("_id", (_type) => ID) { _id }: User
  // ): Promise<User | null> {
  //   const user = await User.findOne({ where: [{ _id }] });
  //   return user;
  // }
}

import { Arg, ID, Mutation, Query, Resolver } from "type-graphql";
import { FindOneOptions } from "typeorm";
import { TableScore } from "../entities/TableScore";
import { createTableScoreInput } from "../types/TableScore/CreateTableScoreInput";
import { TableScoreMutationResponse } from "../types/TableScore/TableScoreMutationResponse";

@Resolver()
export class TableScoreResolver {
  @Mutation((_return) => TableScoreMutationResponse)
  async createTableScore(
    @Arg("createTableScoreInput") args: createTableScoreInput
  ): Promise<TableScoreMutationResponse> {
    try {
      const { name, multiplier, type } = args;

      const newTableScore = TableScore.create({
        name,
        multiplier,
        type,
      });

      await newTableScore.save();

      return {
        code: 200,
        success: true,
        message: "Table score create successfully",
        // user: newUser,
        tableScore: newTableScore,
      };
    } catch (error) {
      console.log(error);
      return {
        code: 500,
        success: false,
        message: `Internal server error, ${error.message}`,
      };
    }
  }

  //   @Mutation((_return) => UserMutationResponse)
  //   async login(
  //     @Arg("loginInput")
  //     { email, password }: LoginInput,
  //     @Ctx() { res }: Context
  //   ): Promise<UserMutationResponse> {
  //     try {
  //       const existingUser = await User.findOne({
  //         where: {
  //           email,
  //         },
  //       });

  //       if (!existingUser) {
  //         return {
  //           code: 400,
  //           success: false,
  //           message: "User not found",
  //         };
  //       }

  //       const isPasswordValid = await argon2.verify(
  //         existingUser.password,
  //         password
  //       );

  //       if (!isPasswordValid) {
  //         return {
  //           code: 400,
  //           success: false,
  //           message: "Incorrect password",
  //         };
  //       }

  //       sendRefreshToken(res, existingUser);

  //       return {
  //         code: 200,
  //         success: true,
  //         message: "Logged in successfully",
  //         user: existingUser,
  //         accessToken: createToken("accessToken", existingUser),
  //       };
  //     } catch (error) {
  //       console.log(error);
  //       return {
  //         code: 500,
  //         success: false,
  //         message: `Internal server error, ${error.message}`,
  //       };
  //     }
  //   }

  //   @UseMiddleware(checkAuth)
  //   @Mutation((_return) => TableScoreMutationResponse)
  //   async logout(
  //     @Ctx() { res, user }: Context
  //   ): Promise<TableScoreMutationResponse> {
  //     const existingUser = await User.findOne({
  //       where: {
  //         _id: new ObjectId(user._id),
  //       },
  //     });

  //     if (!existingUser) {
  //       return {
  //         code: 400,
  //         success: false,
  //         message: "Who are u ?",
  //       };
  //     }

  //     existingUser.token_version += 1;

  //     await existingUser.save();

  //     res.clearCookie("refreshToken", {
  //       httpOnly: true,
  //       secure: true,
  //       sameSite: "lax",
  //       path: "/refresh_token",
  //     });

  //     return {
  //       code: 200,
  //       success: true,
  //       message: "Loggout successfully",
  //     };
  //   }

  //   @UseMiddleware(checkAuth)
  //   @Query((_return) => User, { nullable: true })
  //   async me(@Ctx() { user }: Context): Promise<User | null> {
  //     const existingUser = await User.findOne({
  //       where: {
  //         _id: new ObjectId(user._id),
  //       },
  //     });

  //     return existingUser;
  //   }

  //   @UseMiddleware(checkAuth)
  @Query((_return) => TableScore, { nullable: true })
  async getTableScoreById(
    @Arg("_id", (_type) => ID) _id: string
  ): Promise<TableScore | null> {
    return await TableScore.findOne(_id as FindOneOptions<TableScore>);
  }

  //   @Query((_return) => User, { nullable: true })
  //   async getUserById(
  //     @Arg("_id", (_type) => ID) { _id }: User
  //   ): Promise<User | null> {
  //     const user = await User.findOne({ where: [{ _id }] });
  //     return user;
  //   }
}
